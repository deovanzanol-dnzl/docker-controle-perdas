#!/bin/bash
echo on

echo '###########################################################'
echo '#Script para buildar os repositorios do projeto da rumo.  #'
echo '#                                                         #'
echo '#Building images local                                    #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo '#Building docker controleperdas                           #'
echo '###########################################################'
cd controleperdas
docker build -t controleperdas .
echo '###########################################################'
echo '#Building docker controleperdasconfiguracoes              #'
echo '###########################################################'
cd ../controleperdasconfiguracoes
docker build -t controleperdasconfiguracoes .
echo '###########################################################'
echo '#Building docker sindicancias                             #'
echo '###########################################################'
cd ../sindicancias
docker build -t sindicancias .
echo '###########################################################'
echo '#Building docker comumferroviasauxiliares                 #'
echo '###########################################################'
cd ../comumferroviasauxiliares
docker build -t comumferroviasauxiliares .
echo '###########################################################'
echo '#Building docker cadastros                                #'
echo '###########################################################'
cd ../cadastros
docker build -t cadastros .
echo '###########################################################'
echo '#Building docker malhas                                   #'
echo '###########################################################'
cd    ../malhas
docker build -t malhas .
# echo '###########################################################'
# echo '#Building docker notificacoes                             #'
# echo '###########################################################'
# cd    ../notificacoes
# docker build -t notificacoes .
echo '###########################################################'
echo '#Repositorios Building com sucesso!                       #'
echo '###########################################################'
sleep 10s
