# Executar ambientes local

## Ambientes que são executados
 
 * controleperdas -> http://localhost:8039
 * controleperdasconfiguracoes -> http://localhost:8041
 * sindicancias -> http://localhost:8070
 * comumferroviasauxiliares -> http://localhost:8030
 * cadastros -> http://localhost:8045
 * malhas -> http://localhost:8064

# Passos

Para atualizar as imagens de todos os repositorios e gerar a imagem docker, deve ser executado os scripts, respectivamente:

 * sh auto-atualizar.sh

 * sh build-backend-docker.sh

Com isso, será gerado a image docker para cada microsserviço.

Para inicializar os docker, navegue para o diretório 'controle-perdas-compose' e execute:

* docker-compose up

# Fim!